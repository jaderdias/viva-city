package index

import (
	"appengine"
	"time"
)

type Hit struct {
	Instant  time.Time
	RawQuery string
}

type Sticker struct {
	ShortUrl string
	Location appengine.GeoPoint
}

type Attraction struct {
	Title            string
	Type             string
	Shortdescription string
	Calendarsummary  string
	Address          string
	Location         appengine.GeoPoint
	Url              string
}

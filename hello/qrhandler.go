package index

import (
	"appengine"
	"appengine/datastore"
	"appengine/urlfetch"
	"encoding/json"
	"fmt"
	"html/template"
	"net/http"
	"strings"
)

var qrTemplate = template.Must(template.ParseFiles("templates/qrenc3.html"))

func qrHandler(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	client := urlfetch.Client(c)
	sticker := Sticker{}
	key, err := datastore.Put(c, datastore.NewIncompleteKey(c, "sticker", nil), &sticker)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	requestJson := fmt.Sprintf("{\"longUrl\": \"http://travel-guide-1109.appspot.com/%d\"}", key.IntID())
	requestBody := strings.NewReader(requestJson)
	resp, err := client.Post(
		"https://www.googleapis.com/urlshortener/v1/url?fields=id&key=AIzaSyDL4CuWWO6CSv1OvkzF9oyMAXANWBloUOM",
		"application/json",
		requestBody)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	dec := json.NewDecoder(resp.Body)
	type UrlShortenerResponse struct {
		Id string
	}

	var m UrlShortenerResponse
	err = dec.Decode(&m)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	sticker.ShortUrl = m.Id
	key, err = datastore.Put(c, key, &sticker)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	err = qrTemplate.Execute(w, &(m.Id))
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

package index

import (
	"appengine"
	"appengine/datastore"
	"html/template"
	"net/http"
	"time"
)

var indexTemplate = template.Must(template.ParseFiles("templates/index.html"))

func init() {
	http.HandleFunc("/", handler)
	http.HandleFunc("/qr", qrHandler)
	http.HandleFunc("/upload", uploadHandler)
}

func handler(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	if r.URL.RawQuery != "" {
		hit := Hit{
			Instant:  time.Now(),
			RawQuery: r.URL.RawQuery,
		}

		_, err := datastore.Put(c, datastore.NewIncompleteKey(c, "hit", nil), &hit)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}

	err := indexTemplate.Execute(w, nil)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

package index

import (
	"appengine"
	"appengine/datastore"
	"encoding/json"
	"html/template"
	"net/http"
	"strconv"
	"strings"
)

var uploadTemplate = template.Must(template.ParseFiles("templates/upload.html"))

func uploadHandler(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	if r.Method == "POST" {
		file, _, err := r.FormFile("json")
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		dec := json.NewDecoder(file)
		var places []Place
		err = dec.Decode(&places)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		for _, place := range places {
			if place.Title == "" {
				http.Error(w, "empty title", http.StatusInternalServerError)
				return
			}

			lat, err := strconv.ParseFloat(strings.Replace(place.Location.Latitude, ",", ".", 1), 64)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}

			lng, err := strconv.ParseFloat(strings.Replace(place.Location.Longitude, ",", ".", 1), 64)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}

			query := datastore.NewQuery("Attraction").
				Filter("Title =", place.Title).
				KeysOnly()

			var attractions []Attraction
			keys, err := query.GetAll(c, &attractions)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}

			err = datastore.DeleteMulti(c, keys)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}

			key := datastore.NewIncompleteKey(c, "Attraction", nil)
			attraction := Attraction{
				Title: place.Title,
				Location: appengine.GeoPoint{
					Lat: lat,
					Lng: lng,
				},
				Address:          place.Location.Adress,
				Shortdescription: place.Details["en"].Shortdescription,
			}

			_, err = datastore.Put(c, key, &attraction)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
		}

		return
	}

	err := uploadTemplate.Execute(w, nil)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

type Detail struct {
	Title            string
	Calendarsummary  string
	Shortdescription string
}

type Type struct {
	Type string
}

type Location struct {
	Adress    string
	Latitude  string
	Longitude string
}

type Place struct {
	Title    string
	Details  map[string]Detail
	Types    []Type
	Location Location
	Urls     []string
}
